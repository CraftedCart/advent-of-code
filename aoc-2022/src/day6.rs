fn start_of_packet_index(data: &[u8]) -> usize {
    data.windows(4)
        .enumerate()
        .skip_while(|window| {
            window.1[0] == window.1[1]
                || window.1[0] == window.1[2]
                || window.1[0] == window.1[3]
                || window.1[1] == window.1[2]
                || window.1[1] == window.1[3]
                || window.1[2] == window.1[3]
        })
        .next()
        .unwrap()
        .0
        + 4
}

fn start_of_message_index(data: &[u8]) -> usize {
    data.windows(14)
        .enumerate()
        .skip_while(|window| {
            for i in 0..13 {
                for j in i + 1..14 {
                    if window.1[i] == window.1[j] {
                        return true;
                    }
                }
            }

            false
        })
        .next()
        .unwrap()
        .0
        + 14
}

#[aoc(day6, part1)]
pub fn solve_part1(input: &[u8]) -> usize {
    start_of_packet_index(input)
}

#[aoc(day6, part2)]
pub fn solve_part2(input: &[u8]) -> usize {
    start_of_message_index(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn start_of_packet_bvwbjplbgvbhsrlpgdmjqwftvncz() {
        assert_eq!(start_of_packet_index(b"bvwbjplbgvbhsrlpgdmjqwftvncz"), 5);
    }

    #[test]
    fn start_of_message_mjqjpqmgbljsphdztnvjfqwrcgsmlb() {
        assert_eq!(
            start_of_message_index(b"mjqjpqmgbljsphdztnvjfqwrcgsmlb"),
            19
        );
    }
}
