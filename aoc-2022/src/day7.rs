use std::collections::HashMap;

pub struct Dir {
    children: HashMap<String, FsEntry>,
}

impl Dir {
    fn new() -> Self {
        Self {
            children: HashMap::new(),
        }
    }

    fn size(&self) -> u64 {
        self.children.values().map(FsEntry::size).sum()
    }
}

enum FsEntry {
    Dir(Dir),
    File { size: u64 },
}

impl FsEntry {
    fn size(&self) -> u64 {
        match self {
            FsEntry::Dir(dir) => dir.size(),
            FsEntry::File { size } => *size,
        }
    }
}

fn parse_dir<'a>(dir: &mut Dir, iter_commands_and_output: &mut impl Iterator<Item = &'a str>) {
    while let Some(command_and_output) = iter_commands_and_output.next() {
        let mut lines = command_and_output.lines();
        let command = lines.next().unwrap();
        let output = lines;

        match command {
            "ls" => {
                dir.children = output
                    .map(|line| {
                        let (size_or_dir, name) = line.split_once(" ").unwrap();
                        if size_or_dir == "dir" {
                            (name.to_owned(), FsEntry::Dir(Dir::new()))
                        } else {
                            let size = size_or_dir.parse::<u64>().unwrap();
                            (name.to_owned(), FsEntry::File { size })
                        }
                    })
                    .collect();
            }
            command => {
                // It's a `cd`
                let target = &command[3..];
                match target {
                    ".." => {
                        return;
                    }
                    target => {
                        let subdir = match dir
                            .children
                            .entry(target.to_owned())
                            .or_insert_with(|| FsEntry::Dir(Dir::new()))
                        {
                            FsEntry::Dir(dir) => dir,
                            FsEntry::File { .. } => panic!(),
                        };
                        parse_dir(subdir, iter_commands_and_output);
                    }
                }
            }
        }
    }
}

/// If the size of `dir` is <= 100000, add the size of the dir to `out_sum`
///
/// # Returns
/// Size of the dir
fn find_dirs_smaller_than_100000(dir: &Dir, out_sum: &mut u64) -> u64 {
    let mut dir_size = 0;
    for child in dir.children.values() {
        dir_size += match child {
            FsEntry::Dir(subdir) => find_dirs_smaller_than_100000(subdir, out_sum),
            FsEntry::File { size } => *size,
        };
    }

    if dir_size <= 100000 {
        *out_sum += dir_size;
    }

    dir_size
}

/// If the size of `dir` is >= `min_size` and < `out_best`, set `out_best` to the size of `dir`
///
/// # Returns
/// Size of the dir
fn find_smallest_dir_lte(dir: &Dir, min_size: u64, out_best: &mut u64) -> u64 {
    let mut dir_size = 0;
    for child in dir.children.values() {
        dir_size += match child {
            FsEntry::Dir(subdir) => find_smallest_dir_lte(subdir, min_size, out_best),
            FsEntry::File { size } => *size,
        };
    }

    if dir_size > min_size && dir_size <= *out_best {
        *out_best = dir_size;
    }

    dir_size
}

#[aoc_generator(day7)]
pub fn input_generator(input: &str) -> Dir {
    let mut root = Dir::new();

    // Lazy hack: Skip the first line which is `cd /`
    parse_dir(&mut root, &mut input.split("\n$ ").skip(1));

    root
}

#[aoc(day7, part1)]
pub fn solve_part1(input: &Dir) -> u64 {
    let mut sum = 0;
    find_dirs_smaller_than_100000(input, &mut sum);

    sum
}

#[aoc(day7, part2)]
pub fn solve_part2(input: &Dir) -> u64 {
    let used_size = input.size();
    let unused_size = 70000000 - used_size;
    let space_to_free = 30000000 - unused_size;

    let mut best_size = u64::MAX;
    find_smallest_dir_lte(input, space_to_free, &mut best_size);

    best_size
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn mrowl() {
        const INPUT: &str = "$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k";

        let root = input_generator(INPUT);

        let total_size = root.size();
        assert_eq!(total_size, 48381165);

        let mut part1_answer = 0;
        find_dirs_smaller_than_100000(&root, &mut part1_answer);
        assert_eq!(part1_answer, 95437);
    }
}
