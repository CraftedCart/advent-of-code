use std::collections::HashSet;

type ItemType = u8;

pub struct Rucksack {
    compartment_a: HashSet<ItemType>,
    compartment_b: HashSet<ItemType>,
}

impl Rucksack {
    /// Return the item type shared between compartments A and B
    fn shared_item(&self) -> ItemType {
        *HashSet::intersection(&self.compartment_a, &self.compartment_b)
            .next()
            .unwrap()
    }

    /// Iterate over all items in compartments A and B
    /// Does not guarantee it will be duplicate-free
    fn iter_all_items(&self) -> impl Iterator<Item = ItemType> + '_ {
        self.compartment_a
            .iter()
            .copied()
            .chain(self.compartment_b.iter().copied())
    }
}

fn item_priority(item: u8) -> u32 {
    match item {
        b'a'..=b'z' => (item - b'a') as u32 + 1,
        b'A'..=b'Z' => (item - b'A') as u32 + 27,
        _ => panic!(),
    }
}

/// Return the item shared between a group of 3 rucksacks
fn shared_item_between_sacks(sacks: &[Rucksack]) -> ItemType {
    assert_eq!(sacks.len(), 3);

    let sack_a = HashSet::<ItemType>::from_iter(sacks[0].iter_all_items());
    let sack_b = HashSet::<ItemType>::from_iter(sacks[1].iter_all_items());
    let sack_c = HashSet::<ItemType>::from_iter(sacks[2].iter_all_items());

    let intersect_a_b: HashSet<u8> = HashSet::intersection(&sack_a, &sack_b).copied().collect();
    *HashSet::intersection(&intersect_a_b, &sack_c)
        .next()
        .unwrap()
}

#[aoc_generator(day3)]
pub fn input_generator(input: &[u8]) -> Vec<Rucksack> {
    input
        .split(|c| *c == b'\n')
        .map(|line| {
            let compartment_size = line.len() / 2;
            Rucksack {
                compartment_a: HashSet::from_iter(line[0..compartment_size].iter().copied()),
                compartment_b: HashSet::from_iter(line[compartment_size..].iter().copied()),
            }
        })
        .collect()
}

#[aoc(day3, part1)]
pub fn solve_part1(input: &[Rucksack]) -> u32 {
    input
        .iter()
        .map(|sack| item_priority(sack.shared_item()))
        .sum()
}

#[aoc(day3, part2)]
pub fn solve_part2(input: &[Rucksack]) -> u32 {
    input
        .chunks(3)
        .map(|chunk| item_priority(shared_item_between_sacks(chunk)))
        .sum()
}
