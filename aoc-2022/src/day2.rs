#[derive(PartialEq, Eq, Clone, Copy)]
enum Rps {
    Rock,
    Paper,
    Scissors,
}

impl Rps {
    fn score(&self) -> u32 {
        match self {
            Rps::Rock => 1,
            Rps::Paper => 2,
            Rps::Scissors => 3,
        }
    }

    fn weakness(&self) -> Rps {
        match self {
            Rps::Rock => Rps::Paper,
            Rps::Paper => Rps::Scissors,
            Rps::Scissors => Rps::Rock,
        }
    }

    fn strength(&self) -> Rps {
        match self {
            Rps::Rock => Rps::Scissors,
            Rps::Paper => Rps::Rock,
            Rps::Scissors => Rps::Paper,
        }
    }
}

impl TryFrom<&str> for Rps {
    type Error = ();

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value {
            "A" | "X" => Ok(Rps::Rock),
            "B" | "Y" => Ok(Rps::Paper),
            "C" | "Z" => Ok(Rps::Scissors),
            _ => Err(()),
        }
    }
}

enum GameResult {
    Win,
    Draw,
    Lose,
}

impl GameResult {
    fn score(&self) -> u32 {
        match self {
            GameResult::Win => 6,
            GameResult::Draw => 3,
            GameResult::Lose => 0,
        }
    }
}

impl TryFrom<&str> for GameResult {
    type Error = ();

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value {
            "X" => Ok(GameResult::Lose),
            "Y" => Ok(GameResult::Draw),
            "Z" => Ok(GameResult::Win),
            _ => Err(()),
        }
    }
}

pub struct GamePart1 {
    they_play: Rps,
    we_play: Rps,
}

impl GamePart1 {
    fn result(&self) -> GameResult {
        match (&self.they_play, &self.we_play) {
            (Rps::Rock, Rps::Paper) => GameResult::Win,
            (Rps::Paper, Rps::Scissors) => GameResult::Win,
            (Rps::Scissors, Rps::Rock) => GameResult::Win,
            (they, we) if they == we => GameResult::Draw,
            _ => GameResult::Lose,
        }
    }
}

pub struct GamePart2 {
    they_play: Rps,
    result: GameResult,
}

impl GamePart2 {
    fn we_play(&self) -> Rps {
        match self.result {
            GameResult::Win => self.they_play.weakness(),
            GameResult::Draw => self.they_play,
            GameResult::Lose => self.they_play.strength(),
        }
    }
}

#[aoc_generator(day2, part1)]
pub fn input_generator_part1(input: &str) -> Vec<GamePart1> {
    input
        .lines()
        .map(|line| {
            let (they_play, we_play) = line.split_once(' ').unwrap();
            GamePart1 {
                they_play: they_play.try_into().unwrap(),
                we_play: we_play.try_into().unwrap(),
            }
        })
        .collect()
}

#[aoc(day2, part1)]
pub fn solve_part1(input: &[GamePart1]) -> u32 {
    input.iter().fold(0, |acc, game| {
        acc + game.result().score() + game.we_play.score()
    })
}

#[aoc_generator(day2, part2)]
pub fn input_generator_part2(input: &str) -> Vec<GamePart2> {
    input
        .lines()
        .map(|line| {
            let (they_play, result) = line.split_once(' ').unwrap();
            GamePart2 {
                they_play: they_play.try_into().unwrap(),
                result: result.try_into().unwrap(),
            }
        })
        .collect()
}

#[aoc(day2, part2)]
pub fn solve_part2(input: &[GamePart2]) -> u32 {
    input.iter().fold(0, |acc, game| {
        acc + game.result.score() + game.we_play().score()
    })
}
