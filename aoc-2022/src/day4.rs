use std::ops::RangeInclusive;

pub struct Pair {
    range_a: RangeInclusive<u32>,
    range_b: RangeInclusive<u32>,
}

fn parse_range(string: &str) -> RangeInclusive<u32> {
    let (left, right) = string.split_once('-').unwrap();
    RangeInclusive::new(left.parse::<u32>().unwrap(), right.parse::<u32>().unwrap())
}

fn range_fully_contains_range(a: &RangeInclusive<u32>, b: &RangeInclusive<u32>) -> bool {
    (a.contains(b.start()) && a.contains(b.end())) || (b.contains(a.start()) && b.contains(a.end()))
}

fn range_partially_contains_range(a: &RangeInclusive<u32>, b: &RangeInclusive<u32>) -> bool {
    a.contains(b.start()) || a.contains(b.end()) || b.contains(a.start()) || b.contains(a.end())
}

#[aoc_generator(day4)]
pub fn input_generator(input: &str) -> Vec<Pair> {
    input
        .lines()
        .map(|line| {
            let (left_str, right_str) = line.split_once(',').unwrap();
            Pair {
                range_a: parse_range(left_str),
                range_b: parse_range(right_str),
            }
        })
        .collect()
}

#[aoc(day4, part1)]
pub fn solve_part1(input: &[Pair]) -> usize {
    input
        .iter()
        .filter(|pair| range_fully_contains_range(&pair.range_a, &pair.range_b))
        .count()
}

#[aoc(day4, part2)]
pub fn solve_part2(input: &[Pair]) -> usize {
    input
        .iter()
        .filter(|pair| range_partially_contains_range(&pair.range_a, &pair.range_b))
        .count()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn mrowl() {
        const INPUT: &str = "2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8";
        let pairs = input_generator(INPUT);
        let bad_pairs = solve_part1(&pairs);

        assert_eq!(bad_pairs, 2);
    }
}
