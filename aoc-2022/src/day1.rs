#[derive(Clone)]
pub struct Elf {
    food_calories: Vec<u32>,
}

impl Elf {
    fn total_calories(&self) -> u32 {
        self.food_calories.iter().sum()
    }
}

#[aoc_generator(day1)]
pub fn input_generator(input: &str) -> Vec<Elf> {
    input
        .split("\n\n")
        .map(|elf_lines| Elf {
            food_calories: elf_lines
                .lines()
                .map(|line| line.parse::<u32>().unwrap())
                .collect(),
        })
        .collect()
}

#[aoc(day1, part1)]
pub fn solve_part1(input: &[Elf]) -> u32 {
    input
        .iter()
        .max_by_key(|elf| elf.total_calories())
        .unwrap()
        .total_calories()
}

#[aoc(day1, part2)]
pub fn solve_part2(input: &[Elf]) -> u32 {
    // Clone input into a mutable vec
    let mut sorted = Vec::new();
    sorted.extend_from_slice(input);

    // Sort
    sorted.sort_by_cached_key(|elf| elf.total_calories());

    // Take last 3 and sum it all
    sorted
        .into_iter()
        .rev()
        .take(3)
        .map(|elf| elf.total_calories())
        .sum()
}
