#[derive(Default, Clone)]
struct Stack(Vec<char>);

#[derive(Clone)]
struct Instruction {
    from: usize,
    to: usize,
    count: usize,
}

#[derive(Clone)]
pub struct Vm {
    stacks: [Stack; 9],
    instructions: Vec<Instruction>,
}

impl Vm {
    fn exec_9000(&mut self) {
        for instr in &self.instructions {
            let (stack_from, stack_to) = borrow2(&mut self.stacks, instr.from, instr.to);
            stack_to.0.extend(
                stack_from
                    .0
                    .drain((stack_from.0.len() - instr.count)..)
                    .rev(),
            );
        }
    }

    fn exec_9001(&mut self) {
        for instr in &self.instructions {
            let (stack_from, stack_to) = borrow2(&mut self.stacks, instr.from, instr.to);
            stack_to
                .0
                .extend(stack_from.0.drain((stack_from.0.len() - instr.count)..));
        }
    }
}

/// Borrow 2 items from a slice
///
/// # Panics
/// - If index1 == index2
/// - If an index if out-of-bounds
fn borrow2<'a, T>(slice: &'a mut [T], index1: usize, index2: usize) -> (&'a mut T, &'a mut T) {
    assert_ne!(
        index1, index2,
        "Cannot mutably borrow the same item out of a slice twice",
    );
    let slice_len = slice.len();
    assert!(
        index1 < slice_len,
        "index1 ({index1}) out of bounds (slice is size {slice_len})",
    );
    assert!(
        index2 < slice_len,
        "index2 ({index1}) out of bounds (slice is size {slice_len})",
    );

    let ptr = slice.as_mut_ptr();

    // SAFETY
    // We assert index1 != index2 above, and that both indices are in-bounds
    // So we're certain we're borrowing valid memory, and not mutably borrowing overlapping memory
    unsafe { (&mut *ptr.offset(index1 as _), &mut *ptr.offset(index2 as _)) }
}

#[aoc_generator(day5)]
pub fn input_generator(input: &str) -> Vm {
    let (stacks_str, instructions_str) = input.split_once("\n\n").unwrap();

    // Parse stack image
    let mut stacks: [Stack; 9] = Default::default();
    for line in stacks_str.lines().rev().skip(1) {
        for i in 0..9 {
            match line.chars().nth((i * 4) + 1).unwrap() {
                ' ' => (),
                item => stacks[i].0.push(item),
            }
        }
    }

    // Parse instructions
    let instructions = instructions_str
        .lines()
        .map(|line| {
            let mut split = line.split(" ").skip(1);
            let count = split.next().unwrap().parse::<usize>().unwrap();

            let mut split = split.skip(1);
            let from = split.next().unwrap().parse::<usize>().unwrap() - 1; // Convert 1-indexed to 0-indexed

            let mut split = split.skip(1);
            let to = split.next().unwrap().parse::<usize>().unwrap() - 1; // Convert 1-indexed to 0-indexed

            Instruction { from, to, count }
        })
        .collect();

    Vm {
        stacks,
        instructions,
    }
}

#[aoc(day5, part1)]
pub fn solve_part1(input: &Vm) -> String {
    let mut vm = input.clone();
    vm.exec_9000();

    vm.stacks
        .iter()
        .map(|stack| stack.0.last().unwrap())
        .collect()
}

#[aoc(day5, part2)]
pub fn solve_part2(input: &Vm) -> String {
    let mut vm = input.clone();
    vm.exec_9001();

    vm.stacks
        .iter()
        .map(|stack| stack.0.last().unwrap())
        .collect()
}
