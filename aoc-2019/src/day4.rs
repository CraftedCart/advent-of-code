use std::ops::Range;

#[aoc_generator(day4)]
pub fn input_generator(input: &str) -> Range<u32> {
    let mut split = input.split("-");

    let start = split.next().unwrap().parse::<u32>().unwrap();
    let end = split.next().unwrap().parse::<u32>().unwrap() + 1; // Ranges in Rust have an exclusive end

    Range { start, end }
}

// Just taken from https://stackoverflow.com/questions/41536479/splitting-an-integer-into-individual-digits
fn digits_of(n: u32) -> Vec<u32> {
    fn x_inner(n: u32, xs: &mut Vec<u32>) {
        if n >= 10 {
            x_inner(n / 10, xs);
        }
        xs.push(n % 10);
    }
    let mut xs = Vec::new();
    x_inner(n, &mut xs);
    xs
}

fn has_adjacent_digits(digits: &[u32]) -> bool {
    let mut prev_digit = 10; // 10 is not a valid digit
    for digit in digits {
        if *digit == prev_digit {
            return true;
        }

        prev_digit = *digit;
    }

    false
}

fn has_no_decreasing_digits(digits: &[u32]) -> bool {
    let mut prev_digit: i32 = -1;
    for digit in digits {
        if (*digit as i32) < prev_digit {
            return false;
        }

        prev_digit = *digit as i32;
    }

    true
}

#[aoc(day4, part1)]
pub fn solve_part1(input: &Range<u32>) -> u32 {
    let mut range = input.clone(); // Clone the input so we have mutability
    let mut num_matches = 0;

    while let Some(idx) = range.next() {
        let digits = digits_of(idx);
        if !has_adjacent_digits(&digits) { continue; }
        if !has_no_decreasing_digits(&digits) { continue; }

        num_matches += 1;
    }

    num_matches
}

fn has_two_adjacent_digits(digits: &[u32]) -> bool {
    let mut prev_digit = 10; // 10 is not a valid digit
    let mut num_consecutive = 0;
    for digit in digits {
        if *digit == prev_digit {
            num_consecutive += 1;
        } else {
            if num_consecutive == 1 {
                return true;
            }
            num_consecutive = 0;
        }

        prev_digit = *digit;
    }

    num_consecutive == 1
}

#[aoc(day4, part2)]
pub fn solve_part2(input: &Range<u32>) -> u32 {
    let mut range = input.clone(); // Clone the input so we have mutability
    let mut num_matches = 0;

    while let Some(idx) = range.next() {
        let digits = digits_of(idx);
        if !has_two_adjacent_digits(&digits) { continue; }
        if !has_no_decreasing_digits(&digits) { continue; }

        num_matches += 1;
    }

    num_matches
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_valid_two_consecutive() {
        assert!(has_two_adjacent_digits(&[1, 1, 2, 2, 3, 3]));
        assert!(has_two_adjacent_digits(&[1, 1, 1, 1, 2, 2]));
    }

    #[test]
    fn test_invalid_too_many_consecutive() {
        assert!(!has_two_adjacent_digits(&[1, 2, 3, 4, 4, 4]));
    }
}
