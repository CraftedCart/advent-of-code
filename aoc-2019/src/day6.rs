pub struct Link {
    pub from: String,
    pub to: String,
}

#[aoc_generator(day6)]
pub fn input_generator(input: &str) -> Vec<Link> {
    input
        .lines()
        .map(|line| {
            let mut iter = line.split(")");
            let from = String::from(iter.next().unwrap());
            let to = String::from(iter.next().unwrap());

            Link { from, to }
        })
        .collect()
}

#[aoc(day6, part1)]
pub fn solve_part1(input: &[Link]) -> u32 {
    let mut num_links = 0;

    for link in input {
        num_links += 1;
        let mut parent_name = &link.from;
        loop {
            let parent = input.iter().find(|e| e.to == *parent_name);
            if let Some(parent) = parent {
                num_links += 1;
                parent_name = &parent.from;
            } else {
                break;
            }
        }
    }

    num_links
}

#[aoc(day6, part2)]
pub fn solve_part2(input: &[Link]) -> u32 {
    let target = input.iter().find(|e| e.to == "SAN").unwrap();

    let mut solution_lengths: Vec<u32> = Vec::new();
    let mut visited: Vec<String> = Vec::new();

    fn find_solutions(from: &str, target: &str, cur_length: u32, out: &mut Vec<u32>, input: &[Link], visited: &mut Vec<String>) {
        if visited.iter().find(|e| *e == &from).is_some() {
            return;
        } else {
            visited.push(String::from(from));
        }

        if from == target {
            println!("FOUND");
            out.push(cur_length);
        }

        for elem in input {
            if elem.from == from {
                find_solutions(&elem.to, target, cur_length + 1, out, input, visited)
            } else if elem.to == from {
                find_solutions(&elem.from, target, cur_length + 1, out, input, visited)
            }
        }
    }

    find_solutions("YOU", &target.from, 0, &mut solution_lengths, &input, &mut visited);

    // - 1 as we don't count the transfer from YOU to what we're orbiting around
    solution_lengths.into_iter().fold(std::u32::MAX, |min, x| if x < min { x } else { min }) - 1
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example_map() {
        const MAP: &str = "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L";
        let planets = input_generator(MAP);
        let links = solve_part1(&planets);

        assert_eq!(links, 42);
    }

    #[test]
    fn test_single_orbit() {
        const MAP: &str = "AAA)BBB";
        let planets = input_generator(MAP);
        let links = solve_part1(&planets);

        assert_eq!(links, 1);
    }

    #[test]
    fn test_four_orbits() {
        const MAP: &str = "AAA)BBB\nBBB)CCC\nCCC)DDD\nDDD)EEE";
        let planets = input_generator(MAP);
        let links = solve_part1(&planets);

        assert_eq!(links, 10);
    }

    #[test]
    fn test_out_of_order() {
        const MAP: &str = "CCC)DDD\nBBB)CCC\nDDD)EEE\nAAA)BBB";
        let planets = input_generator(MAP);
        let links = solve_part1(&planets);

        assert_eq!(links, 10);
    }

    #[test]
    fn test_out_of_order2() {
        const MAP: &str = "DDD)FFF\nAAA)HHH\nCCC)DDD\nDDD)GGG\nBBB)CCC\nDDD)EEE\nAAA)BBB";
        let planets = input_generator(MAP);
        let links = solve_part1(&planets);

        assert_eq!(links, 19);
    }

    #[test]
    fn test_example_transfers() {
        const MAP: &str = "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L\nK)YOU\nI)SAN";
        let planets = input_generator(MAP);
        let xfers = solve_part2(&planets);

        assert_eq!(xfers, 4);
    }
}
