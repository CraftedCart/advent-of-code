use std::collections::VecDeque;

pub struct Interpreter {
    pub mem: Vec<i32>,
    program_counter: usize,
    input_queue: VecDeque<i32>,
    pub output_queue: VecDeque<i32>,
}

impl Interpreter {
    pub fn new_from_mem(mem: &[i32]) -> Interpreter {
        Interpreter {
            mem: Vec::from(mem),
            program_counter: 0,
            input_queue: VecDeque::new(),
            output_queue: VecDeque::new(),
        }
    }

    pub fn queue_input(&mut self, input: i32) {
        self.input_queue.push_back(input);
    }

    pub fn run(&mut self) {
        loop {
            let instr = self.parse_instruction(self.program_counter);
            if !self.exec_instruction(&instr) {
                return;
            }
        }
    }

    fn parse_instruction(&self, location: usize) -> Instruction {
        let opcode_and_modes = self.mem[location];

        let opcode = opcode_and_modes % 100;
        let param1_mode = (opcode_and_modes % 1000 - opcode) / 100;
        let param2_mode = (opcode_and_modes % 10000 - param1_mode - opcode) / 1000;
        // let param3_mode = (opcode_and_modes % 100000 - param2_mode - param1_mode - opcode) / 10000;

        let param1_mode = ParamMode::from_int(param1_mode);
        let param2_mode = ParamMode::from_int(param2_mode);
        // let param3_mode = ParamMode::from_int(param3_mode);

        match opcode {
            1 => Instruction::Add {
                in1: self.mem[location + 1],
                in1_mode: param1_mode,
                in2: self.mem[location + 2],
                in2_mode: param2_mode,
                out1: self.mem[location + 3],
            },
            2 => Instruction::Multiply {
                in1: self.mem[location + 1],
                in1_mode: param1_mode,
                in2: self.mem[location + 2],
                in2_mode: param2_mode,
                out1: self.mem[location + 3],
            },
            3 => Instruction::Input {
                out1: self.mem[location + 1],
            },
            4 => Instruction::Output {
                in1: self.mem[location + 1],
                in1_mode: param1_mode,
            },
            5 => Instruction::JumpIfTrue {
                in1: self.mem[location + 1],
                in1_mode: param1_mode,
                in2: self.mem[location + 2],
                in2_mode: param2_mode,
            },
            6 => Instruction::JumpIfFalse {
                in1: self.mem[location + 1],
                in1_mode: param1_mode,
                in2: self.mem[location + 2],
                in2_mode: param2_mode,
            },
            7 => Instruction::LessThan {
                in1: self.mem[location + 1],
                in1_mode: param1_mode,
                in2: self.mem[location + 2],
                in2_mode: param2_mode,
                out1: self.mem[location + 3],
            },
            8 => Instruction::Equals {
                in1: self.mem[location + 1],
                in1_mode: param1_mode,
                in2: self.mem[location + 2],
                in2_mode: param2_mode,
                out1: self.mem[location + 3],
            },
            99 => Instruction::Halt,
            _ => panic!("Unrecognised opcode {}", opcode),
        }
    }

    fn exec_instruction(&mut self, instr: &Instruction) -> bool {
        match instr {
            Instruction::Add {
                in1,
                in1_mode,
                in2,
                in2_mode,
                out1,
            } => {
                let in1 = self.get_input(in1, in1_mode);
                let in2 = self.get_input(in2, in2_mode);
                self.mem[*out1 as usize] = in1 + in2;

                self.program_counter += 4;
            }
            Instruction::Multiply {
                in1,
                in1_mode,
                in2,
                in2_mode,
                out1,
            } => {
                let in1 = self.get_input(in1, in1_mode);
                let in2 = self.get_input(in2, in2_mode);
                self.mem[*out1 as usize] = in1 * in2;

                self.program_counter += 4;
            }
            Instruction::Input { out1 } => {
                let input = self
                    .input_queue
                    .pop_front()
                    .expect("Tried to grab input but the input queue was empty");
                self.mem[*out1 as usize] = input;

                self.program_counter += 2;
            }
            Instruction::Output { in1, in1_mode } => {
                let in1 = self.get_input(in1, in1_mode);
                self.output_queue.push_back(in1);

                self.program_counter += 2;
            }
            Instruction::JumpIfTrue {
                in1,
                in1_mode,
                in2,
                in2_mode,
            } => {
                let in1 = self.get_input(in1, in1_mode);
                let in2 = self.get_input(in2, in2_mode);

                if in1 != 0 {
                    self.program_counter = in2 as usize;
                } else {
                    self.program_counter += 3;
                }
            }
            Instruction::JumpIfFalse {
                in1,
                in1_mode,
                in2,
                in2_mode,
            } => {
                let in1 = self.get_input(in1, in1_mode);
                let in2 = self.get_input(in2, in2_mode);

                if in1 == 0 {
                    self.program_counter = in2 as usize;
                } else {
                    self.program_counter += 3;
                }
            }
            Instruction::LessThan {
                in1,
                in1_mode,
                in2,
                in2_mode,
                out1,
            } => {
                let in1 = self.get_input(in1, in1_mode);
                let in2 = self.get_input(in2, in2_mode);
                self.mem[*out1 as usize] = if in1 < in2 { 1 } else { 0 };

                self.program_counter += 4;
            }
            Instruction::Equals {
                in1,
                in1_mode,
                in2,
                in2_mode,
                out1,
            } => {
                let in1 = self.get_input(in1, in1_mode);
                let in2 = self.get_input(in2, in2_mode);
                self.mem[*out1 as usize] = if in1 == in2 { 1 } else { 0 };

                self.program_counter += 4;
            }
            Instruction::Halt => {
                return false;
            }
        }

        true
    }

    fn get_input(&self, value: &i32, mode: &ParamMode) -> i32 {
        match mode {
            ParamMode::Position => self.mem[*value as usize],
            ParamMode::Immediate => *value,
        }
    }
}

enum Instruction {
    Add {
        in1: i32,
        in1_mode: ParamMode,
        in2: i32,
        in2_mode: ParamMode,
        out1: i32,
    },
    Multiply {
        in1: i32,
        in1_mode: ParamMode,
        in2: i32,
        in2_mode: ParamMode,
        out1: i32,
    },
    Input {
        out1: i32,
    },
    Output {
        in1: i32,
        in1_mode: ParamMode,
    },
    JumpIfTrue {
        in1: i32,
        in1_mode: ParamMode,
        in2: i32,
        in2_mode: ParamMode,
    },
    JumpIfFalse {
        in1: i32,
        in1_mode: ParamMode,
        in2: i32,
        in2_mode: ParamMode,
    },
    LessThan {
        in1: i32,
        in1_mode: ParamMode,
        in2: i32,
        in2_mode: ParamMode,
        out1: i32,
    },
    Equals {
        in1: i32,
        in1_mode: ParamMode,
        in2: i32,
        in2_mode: ParamMode,
        out1: i32,
    },
    Halt,
}

enum ParamMode {
    Position,
    Immediate,
}

impl ParamMode {
    fn from_int(mode: i32) -> ParamMode {
        match mode {
            0 => ParamMode::Position,
            1 => ParamMode::Immediate,
            _ => panic!("Invalid param mode {}", mode),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_add() {
        const CODE: &[i32] = &[1101, 100, -1, 4, 0];

        let mut interpreter = Interpreter::new_from_mem(CODE);
        interpreter.run();

        assert_eq!(interpreter.mem[4], 99);
    }
}
