#[aoc_generator(day1)]
pub fn input_generator(input: &str) -> Vec<f64> {
    input
        .lines()
        .map(|mass| mass.parse::<f64>().unwrap())
        .collect()
}

fn get_fuel_for_mass(mass: &f64) -> i32 {
    ((mass / 3.0).floor() as i32) - 2
}

#[aoc(day1, part1)]
pub fn solve_part1(input: &[f64]) -> i32 {
    input.iter().map(get_fuel_for_mass).sum()
}

#[aoc(day1, part2)]
pub fn solve_part2(input: &[f64]) -> i32 {
    input
        .iter()
        .map(|&mass| {
            let mut fuel = get_fuel_for_mass(&mass);

            // Fuel itself has a mass and thus we require extra fuel
            let mut fuel_for_fuel = get_fuel_for_mass(&(fuel as f64));
            while fuel_for_fuel > 0 {
                fuel += fuel_for_fuel;

                fuel_for_fuel = get_fuel_for_mass(&(fuel_for_fuel as f64));
            }

            fuel
        })
        .sum()
}
