//! This is awful and I hate it

#[derive(PartialEq, Eq, Debug)]
enum Axis {
    Horiz,
    Vert,
}

impl Axis {
    fn from_char(c: char) -> Axis {
        match c {
            'R' | 'L' => Axis::Horiz,
            'U' | 'D' => Axis::Vert,
            _ => panic!("Expected R, L, U, or D"),
        }
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
struct Vector2 {
    x: i32,
    y: i32,
}

impl Vector2 {
    fn new(x: i32, y: i32) -> Vector2 {
        Vector2 { x, y }
    }

    fn manhattan_distance_to(&self, other: &Vector2) -> u32 {
        ((self.x - other.x).abs() + (self.y - other.y).abs()) as u32
    }
}

struct Line {
    from: Vector2,
    axis: Axis,
    dist: i32,
}

impl Line {
    fn iter_points(&self) -> LinePointsIter {
        LinePointsIter { line: self, idx: 0 }
    }
}

// I'm sure this is rather naive but screw it
struct LinePointsIter<'a> {
    line: &'a Line,
    idx: i32,
}

impl<'a> Iterator for LinePointsIter<'a> {
    type Item = Vector2;

    fn next(&mut self) -> Option<Self::Item> {
        if self.idx.abs() > self.line.dist.abs() {
            return None;
        }

        let ret = match self.line.axis {
            Axis::Horiz => {
                Vector2 { x: self.line.from.x + self.idx, y: self.line.from.y }
            }
            Axis::Vert => {
                Vector2 { x: self.line.from.x, y: self.line.from.y + self.idx }
            }
        };

        if self.line.dist > 0 {
            self.idx += 1;
        } else {
            self.idx -= 1;
        }

        Some(ret)
    }
}

pub struct Wire {
    lines: Vec<Line>,
}

#[aoc_generator(day3)]
pub fn input_generator(input: &str) -> Vec<Wire> {
    input
        .lines()
        .map(|wire| {
            let mut cur_point = Vector2::new(0, 0);

            let lines = wire
                .split(",")
                .map(|path| {
                    let dir = path.chars().next().unwrap();
                    let axis = Axis::from_char(dir);

                    let dist_str = &path[1..];
                    let mut dist = dist_str.parse::<i32>().unwrap();

                    // Use negative numbers for dist, if we're doing left or down
                    if dir == 'L' || dir == 'D' {
                        dist *= -1;
                    }

                    let from = cur_point;

                    match &axis {
                        Axis::Horiz => cur_point.x += dist,
                        Axis::Vert => cur_point.y += dist,
                    }

                    Line { from, axis, dist }
                })
                .collect();

            Wire { lines }
        })
        .collect()
}

#[aoc(day3, part1)]
pub fn solve_part1(input: &[Wire]) -> u32 {
    let mut closest_dist = std::u32::MAX;

    // Woefully naive BUT IT WORKS!!
    for line1 in &input[0].lines {
        for line2 in &input[1].lines {
            for point1 in line1.iter_points() {
                for point2 in line2.iter_points() {
                    if point1 == point2 {
                        let dist = point1.manhattan_distance_to(&Vector2::new(0, 0));
                        if dist < closest_dist && dist != 0 {
                            closest_dist = dist;
                        }
                    }
                }
            }
        }
    }

    closest_dist
}

#[aoc(day3, part2)]
pub fn solve_part2(input: &[Wire]) -> i32 {
    let mut shortest_dist = std::i32::MAX;

    // Woefully naive BUT IT WORKS!!
    let mut line1_dist: i32 = 0;
    for line1 in &input[0].lines {
        let mut line2_dist: i32 = 0;
        for line2 in &input[1].lines {
            for (idx1, point1) in line1.iter_points().enumerate() {
                for (idx2, point2) in line2.iter_points().enumerate() {
                    if point1 == point2 {
                        let dist: i32 = line1_dist + line2_dist + idx1 as i32 + idx2 as i32;
                        if dist < shortest_dist && dist != 0 {
                            shortest_dist = dist;
                        }
                    }
                }
            }

            line2_dist += line2.dist.abs()
        }

        line1_dist += line1.dist.abs()
    }

    shortest_dist
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_input_parses_into_correct_wires() {
        const INPUT: &str = "R8,U5,L5,D3\nU7,R6,D4,L4";

        let wires = input_generator(INPUT);
        assert_eq!(wires.len(), 2);

        let wire1 = &wires[0];
        assert_eq!(wire1.lines[0].from, Vector2::new(0, 0));
        assert_eq!(wire1.lines[1].from, Vector2::new(8, 0));
        assert_eq!(wire1.lines[2].from, Vector2::new(8, 5));
        assert_eq!(wire1.lines[3].from, Vector2::new(3, 5));

        assert_eq!(wire1.lines[0].axis, Axis::Horiz);
        assert_eq!(wire1.lines[1].axis, Axis::Vert);
        assert_eq!(wire1.lines[2].axis, Axis::Horiz);
        assert_eq!(wire1.lines[3].axis, Axis::Vert);

        assert_eq!(wire1.lines[0].dist, 8);
        assert_eq!(wire1.lines[1].dist, 5);
        assert_eq!(wire1.lines[2].dist, -5);
        assert_eq!(wire1.lines[3].dist, -3);

        let wire2 = &wires[1];
        assert_eq!(wire2.lines[0].from, Vector2::new(0, 0));
        assert_eq!(wire2.lines[1].from, Vector2::new(0, 7));
        assert_eq!(wire2.lines[2].from, Vector2::new(6, 7));
        assert_eq!(wire2.lines[3].from, Vector2::new(6, 3));

        assert_eq!(wire2.lines[0].axis, Axis::Vert);
        assert_eq!(wire2.lines[1].axis, Axis::Horiz);
        assert_eq!(wire2.lines[2].axis, Axis::Vert);
        assert_eq!(wire2.lines[3].axis, Axis::Horiz);

        assert_eq!(wire2.lines[0].dist, 7);
        assert_eq!(wire2.lines[1].dist, 6);
        assert_eq!(wire2.lines[2].dist, -4);
        assert_eq!(wire2.lines[3].dist, -4);
    }

    #[test]
    fn test_shortest_distance_610() {
        const INPUT: &str = "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83";

        let wires = input_generator(INPUT);
        let dist = solve_part2(&wires);

        assert_eq!(dist, 610);
    }

    #[test]
    fn test_shortest_distance_410() {
        const INPUT: &str = "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7";

        let wires = input_generator(INPUT);
        let dist = solve_part2(&wires);

        assert_eq!(dist, 410);
    }
}
