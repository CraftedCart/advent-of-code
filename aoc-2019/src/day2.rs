#[aoc_generator(day2)]
pub fn input_generator(input: &str) -> Vec<u32> {
    input
        .split(",")
        .map(|num| num.parse::<u32>().unwrap())
        .collect()
}

fn exec_intcode(mem: &mut [u32]) -> u32 {
    let mut program_counter = 0;

    loop {
        let opcode = mem[program_counter];
        match opcode {
            1 => {
                // Add
                let addr1 = mem[program_counter + 1] as usize;
                let addr2 = mem[program_counter + 2] as usize;
                let output_addr = mem[program_counter + 3] as usize;

                let operand1 = mem[addr1];
                let operand2 = mem[addr2];

                mem[output_addr] = operand1 + operand2;
            }
            2 => {
                // Multiply
                let addr1 = mem[program_counter + 1] as usize;
                let addr2 = mem[program_counter + 2] as usize;
                let output_addr = mem[program_counter + 3] as usize;

                let operand1 = mem[addr1];
                let operand2 = mem[addr2];

                mem[output_addr] = operand1 * operand2;
            }
            99 => {
                // Halt
                return mem[0];
            }
            _ => panic!("Unknown opcode {} at PC = {}", opcode, program_counter),
        }

        program_counter += 4;
    }
}

#[aoc(day2, part1)]
pub fn solve_part1(input: &[u32]) -> u32 {
    let mut mem = Vec::from(input);

    mem[1] = 12;
    mem[2] = 2;

    exec_intcode(&mut mem)
}

#[aoc(day2, part2)]
pub fn solve_part2(input: &[u32]) -> u32 {
    let mut mem = Vec::from(input);

    let mut noun = 0;
    let mut verb = 0;
    loop {
        mem[1] = noun;
        mem[2] = verb;

        if exec_intcode(&mut mem) == 19690720 {
            return (100 * noun) + verb;
        }

        // Try combinations of noun 0 - 99 and verb 0 - 99
        noun += 1;
        if noun > 99 {
            noun = 0;
            verb += 1;
        }

        mem = Vec::from(input); // Reset memory
    }
}
