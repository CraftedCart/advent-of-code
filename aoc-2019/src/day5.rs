use crate::intcode::*;

#[aoc_generator(day5)]
pub fn input_generator(input: &str) -> Vec<i32> {
    input
        .split(",")
        .map(|num| num.parse::<i32>().unwrap())
        .collect()
}

#[aoc(day5, part1)]
pub fn solve_part1(input: &[i32]) -> i32 {
    let mut interpreter = Interpreter::new_from_mem(input);
    interpreter.queue_input(1);
    interpreter.run();

    assert!(interpreter.output_queue.len() > 0);
    let result = interpreter.output_queue.pop_back().unwrap();

    // Check that all tests passed (all other outputs were 0)
    for output in interpreter.output_queue {
        assert_eq!(output, 0);
    }

    result
}

#[aoc(day5, part2)]
pub fn solve_part2(input: &[i32]) -> i32 {
    let mut interpreter = Interpreter::new_from_mem(input);
    interpreter.queue_input(5);
    interpreter.run();

    assert_eq!(interpreter.output_queue.len(), 1);
    let result = interpreter.output_queue.pop_back().unwrap();

    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_position_jump() {
        const CODE_STR: &str = "3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9";
        let code = input_generator(CODE_STR);

        // Zero input
        let mut interpreter = Interpreter::new_from_mem(&code);
        interpreter.queue_input(0);
        interpreter.run();

        assert_eq!(interpreter.output_queue.pop_back().unwrap(), 0);

        // Non-zero input
        let mut interpreter = Interpreter::new_from_mem(&code);
        interpreter.queue_input(42);
        interpreter.run();

        assert_eq!(interpreter.output_queue.pop_back().unwrap(), 1);
    }

    #[test]
    fn test_immediate_jump() {
        const CODE_STR: &str = "3,3,1105,-1,9,1101,0,0,12,4,12,99,1";
        let code = input_generator(CODE_STR);

        // Zero input
        let mut interpreter = Interpreter::new_from_mem(&code);
        interpreter.queue_input(0);
        interpreter.run();

        assert_eq!(interpreter.output_queue.pop_back().unwrap(), 0);

        // Non-zero input
        let mut interpreter = Interpreter::new_from_mem(&code);
        interpreter.queue_input(42);
        interpreter.run();

        assert_eq!(interpreter.output_queue.pop_back().unwrap(), 1);
    }

    #[test]
    fn test_cmp8() {
        const CODE_STR: &str = "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99";
        let code = input_generator(CODE_STR);

        let mut interpreter = Interpreter::new_from_mem(&code);
        interpreter.queue_input(7);
        interpreter.run();
        assert_eq!(interpreter.output_queue.pop_back().unwrap(), 999);

        let mut interpreter = Interpreter::new_from_mem(&code);
        interpreter.queue_input(8);
        interpreter.run();
        assert_eq!(interpreter.output_queue.pop_back().unwrap(), 1000);

        let mut interpreter = Interpreter::new_from_mem(&code);
        interpreter.queue_input(16);
        interpreter.run();
        assert_eq!(interpreter.output_queue.pop_back().unwrap(), 1001);
    }
}
