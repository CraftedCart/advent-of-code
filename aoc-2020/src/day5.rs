use std::convert::TryInto;

#[derive(Debug, PartialEq, Eq)]
enum Half {
    Lower,
    Upper,
}

pub struct SeatLoc {
    row_specifier: [Half; 7],
    col_specifier: [Half; 3],
}

impl SeatLoc {
    fn get_row(&self) -> u8 {
        let mut min_val = 0;
        let mut max_val = 128;

        for half in self.row_specifier.iter() {
            let range = max_val - min_val;

            match half {
                Half::Lower => {
                    max_val -= range / 2;
                }
                Half::Upper => {
                    min_val += range / 2;
                }
            }
        }

        assert_eq!(min_val, max_val - 1);
        min_val
    }

    fn get_col(&self) -> u8 {
        let mut min_val = 0;
        let mut max_val = 8;

        for half in self.col_specifier.iter() {
            let range = max_val - min_val;

            match half {
                Half::Lower => {
                    max_val -= range / 2;
                }
                Half::Upper => {
                    min_val += range / 2;
                }
            }
        }

        assert_eq!(min_val, max_val - 1);
        min_val
    }

    fn get_seat_id(&self) -> u32 {
        ((self.get_row() as u32) * 8) + (self.get_col() as u32)
    }

    fn get_seat_id_from_row_col(row: u8, col: u8) -> u32 {
        ((row as u32) * 8) + (col as u32)
    }
}

#[aoc_generator(day5)]
pub fn input_generator(input: &str) -> Vec<SeatLoc> {
    input
        .lines()
        .map(|line| {
            let mut chars = line.chars();

            let row_chars = chars.by_ref().take(7);
            let row_specifier = row_chars
                .map(|ch| match ch {
                    'F' => Half::Lower,
                    'B' => Half::Upper,
                    _ => panic!("Row char was not F/B"),
                })
                .collect::<Vec<Half>>()
                .try_into()
                .unwrap();

            let col_chars = chars.take(3);
            let col_specifier = col_chars
                .map(|ch| match ch {
                    'L' => Half::Lower,
                    'R' => Half::Upper,
                    _ => panic!("Col char was not L/R"),
                })
                .collect::<Vec<Half>>()
                .try_into()
                .unwrap();

            SeatLoc {
                row_specifier,
                col_specifier,
            }
        })
        .collect()
}

#[aoc(day5, part1)]
pub fn solve_part1(input: &[SeatLoc]) -> u32 {
    input.iter().map(|loc| loc.get_seat_id()).max().unwrap()
}

#[aoc(day5, part2)]
pub fn solve_part2(input: &[SeatLoc]) -> u32 {
    let mut filled_seats: [[bool; 8]; 128] = [[false; 8]; 128];

    for loc in input {
        filled_seats[loc.get_row() as usize][loc.get_col() as usize] = true;
    }

    // Find missing seats (we don't need to check missing_from_hi)
    let mut missing_from_lo = 0;

    for row in filled_seats.iter() {
        if !row[0] {
            missing_from_lo += 1;
        } else {
            break;
        }
    }

    // Find our empty seat
    for (row_index, row) in filled_seats.iter().enumerate().skip(missing_from_lo) {
        for (col_index, seat) in row.iter().enumerate() {
            if !seat {
                return SeatLoc::get_seat_id_from_row_col(row_index as u8, col_index as u8);
            }
        }
    }

    panic!("No empty seat found!")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_input_generator() {
        const INPUT_STR: &str = "BFFFBBFRRR";
        let locs = input_generator(INPUT_STR);
        let loc = &locs[0];

        assert_eq!(
            loc.row_specifier,
            [
                Half::Upper,
                Half::Lower,
                Half::Lower,
                Half::Lower,
                Half::Upper,
                Half::Upper,
                Half::Lower,
            ]
        );
        assert_eq!(loc.col_specifier, [Half::Upper, Half::Upper, Half::Upper]);
    }

    #[test]
    fn test_seat_pos() {
        const INPUT_STR: &str = "FBFBBFFRLR";
        let locs = input_generator(INPUT_STR);
        let loc = &locs[0];

        assert_eq!(loc.get_row(), 44);
        assert_eq!(loc.get_col(), 5);
        assert_eq!(loc.get_seat_id(), 357);
    }
}
