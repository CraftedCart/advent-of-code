#[derive(Clone)]
enum Opcode {
    Acc,
    Jmp,
    Nop,
}

#[derive(PartialEq, Eq)]
enum HaltStatus {
    HaltsNormally,
    DoesNotHalt,
    StillRunning,
    Errors,
}

#[derive(Clone)]
pub struct Instr {
    opcode: Opcode,
    value: i32,
}

struct Console<'a> {
    program_counter: isize,
    accumulator: i32,
    code: &'a [Instr],
}

impl<'a> Console<'a> {
    /// Returns how we halt, if we do
    fn exec_one_instr(&mut self) -> HaltStatus {
        let instr = &self.code[self.program_counter as usize];
        match &instr.opcode {
            Opcode::Acc => self.accumulator += instr.value,
            Opcode::Jmp => self.program_counter += (instr.value - 1) as isize,
            Opcode::Nop => (),
        }

        self.program_counter += 1;

        if self.program_counter == self.code.len() as isize {
            HaltStatus::HaltsNormally
        } else if self.program_counter < 0 {
            HaltStatus::Errors
        } else {
            HaltStatus::StillRunning
        }
    }

    fn does_halt(&mut self) -> HaltStatus {
        let mut executed_lines: Vec<bool> = vec![false; self.code.len()];

        loop {
            if executed_lines[self.program_counter as usize] {
                return HaltStatus::DoesNotHalt;
            } else {
                executed_lines[self.program_counter as usize] = true;
            }

            match self.exec_one_instr() {
                HaltStatus::StillRunning => (),
                status => return status,
            }
        }
    }
}

fn split_once<'a>(in_string: &'a str, split_with: &str) -> (&'a str, &'a str) {
    let mut split = in_string.splitn(2, split_with);

    let first = split.next().unwrap();
    let second = split.next().unwrap();
    (first, second)
}

#[aoc_generator(day8)]
pub fn input_generator(input: &str) -> Vec<Instr> {
    input
        .lines()
        .map(|line| {
            let (opcode, value) = split_once(line, " ");

            let opcode = match opcode {
                "acc" => Opcode::Acc,
                "jmp" => Opcode::Jmp,
                "nop" => Opcode::Nop,
                _ => panic!("Invalid opcode"),
            };

            let value = value.parse::<i32>().unwrap();

            Instr { opcode, value }
        })
        .collect()
}

#[aoc(day8, part1)]
pub fn solve_part1(input: &[Instr]) -> i32 {
    let mut executed_lines: Vec<bool> = vec![false; input.len()];

    let mut console = Console {
        program_counter: 0,
        accumulator: 0,
        code: input,
    };

    loop {
        if executed_lines[console.program_counter as usize] {
            return console.accumulator;
        } else {
            executed_lines[console.program_counter as usize] = true;
        }

        console.exec_one_instr();
    }
}

#[aoc(day8, part2)]
pub fn solve_part2(input: &[Instr]) -> i32 {
    let mut input = input.to_vec();

    for i in 0..input.len() {
        match input[i].opcode {
            Opcode::Nop => input[i].opcode = Opcode::Jmp,
            Opcode::Jmp => input[i].opcode = Opcode::Nop,
            _ => continue,
        }

        let mut console = Console {
            program_counter: 0,
            accumulator: 0,
            code: &input,
        };
        if console.does_halt() == HaltStatus::HaltsNormally {
            return console.accumulator;
        }

        // Revert the opcode
        match input[i].opcode {
            Opcode::Nop => input[i].opcode = Opcode::Jmp,
            Opcode::Jmp => input[i].opcode = Opcode::Nop,
            _ => unreachable!(),
        }
    }

    panic!("No solution found")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_double_exec() {
        const INPUT_STR: &str = "nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6";
        let instrs = input_generator(INPUT_STR);

        assert_eq!(solve_part1(&instrs), 5);
    }

    #[test]
    fn test_change_instr() {
        const INPUT_STR: &str = "nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6";
        let instrs = input_generator(INPUT_STR);

        assert_eq!(solve_part2(&instrs), 8);
    }
}
