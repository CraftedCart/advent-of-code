use std::collections::HashMap;

pub struct Group {
    /// Maps questions to the number of people that answered yes to that question
    yes_questions: HashMap<char, u32>,

    /// Number of people in the group
    num_people: u32,
}

impl Group {
    fn new() -> Group {
        Group {
            yes_questions: HashMap::new(),
            num_people: 0,
        }
    }
}

#[aoc_generator(day6)]
pub fn input_generator(input: &str) -> Vec<Group> {
    input
        .split("\n\n")
        .map(|group_str| {
            let mut group = Group::new();

            for person in group_str.lines() {
                for answer in person.chars() {
                    group
                        .yes_questions
                        .entry(answer)
                        .and_modify(|count| *count += 1)
                        .or_insert(1);
                }

                group.num_people += 1;
            }

            group
        })
        .collect()
}

#[aoc(day6, part1)]
pub fn solve_part1(input: &[Group]) -> usize {
    input.iter().map(|group| group.yes_questions.len()).sum()
}

#[aoc(day6, part2)]
pub fn solve_part2(input: &[Group]) -> u32 {
    let mut all_question_count = 0;
    for group in input.iter() {
        for (_question, num_people) in group.yes_questions.iter() {
            if *num_people == group.num_people {
                all_question_count += 1;
            }
        }
    }

    all_question_count
}
