#[aoc_generator(day1)]
pub fn input_generator(input: &str) -> Vec<u32> {
    input
        .lines()
        .map(|line| line.parse::<u32>().unwrap())
        .collect()
}

#[aoc(day1, part1)]
pub fn solve_part1(input: &[u32]) -> u32 {
    for num1 in input {
        for num2 in input {
            if num1 + num2 == 2020 {
                return num1 * num2;
            }
        }
    }

    panic!("No answer found!")
}

#[aoc(day1, part2)]
pub fn solve_part2(input: &[u32]) -> u32 {
    for num1 in input {
        for num2 in input {
            for num3 in input {
                if num1 + num2 + num3 == 2020 {
                    return num1 * num2 * num3;
                }
            }
        }
    }

    panic!("No answer found!")
}
