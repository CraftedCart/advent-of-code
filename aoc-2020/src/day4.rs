use std::collections::HashMap;

const REQUIRED_FIELDS: &[&str] = &[
    "byr", // (Birth Year)
    "iyr", // (Issue Year)
    "eyr", // (Expiration Year)
    "hgt", // (Height)
    "hcl", // (Hair Color)
    "ecl", // (Eye Color)
    "pid", // (Passport ID)
           // Country ID ignored
];

const VALID_HAIR_COLORS: &[&str] = &["amb", "blu", "brn", "gry", "grn", "hzl", "oth"];

pub struct Passport {
    fields: HashMap<String, String>,
}

impl Passport {
    fn has_required_fields(&self) -> bool {
        for field in REQUIRED_FIELDS {
            if !self.fields.contains_key(*field) {
                return false;
            }
        }

        true
    }

    fn is_valid(&self) -> bool {
        match self.fields.get("byr") {
            Some(value) => {
                let value = value.parse::<i32>().unwrap();
                if value < 1920 || value > 2002 {
                    return false;
                }
            }
            None => return false,
        }

        match self.fields.get("iyr") {
            Some(value) => {
                let value = value.parse::<i32>().unwrap();
                if value < 2010 || value > 2020 {
                    return false;
                }
            }
            None => return false,
        }

        match self.fields.get("eyr") {
            Some(value) => {
                let value = value.parse::<i32>().unwrap();
                if value < 2020 || value > 2030 {
                    return false;
                }
            }
            None => return false,
        }

        match self.fields.get("hgt") {
            Some(value) => {
                if value.ends_with("cm") {
                    let cm = value.trim_end_matches("cm").parse::<i32>().unwrap();
                    if cm < 150 || cm > 193 {
                        return false;
                    }
                } else if value.ends_with("in") {
                    let inches = value.trim_end_matches("in").parse::<i32>().unwrap();
                    if inches < 59 || inches > 76 {
                        return false;
                    }
                } else {
                    return false;
                }
            }
            None => return false,
        }

        match self.fields.get("hcl") {
            Some(value) => {
                let mut chars = value.chars();
                if chars.next().unwrap() != '#' {
                    return false;
                }

                let mut remaining_chars = 0;
                for ch in chars {
                    if !((ch >= '0' && ch <= '9') || (ch >= 'a' && ch <= 'f')) {
                        return false;
                    }

                    remaining_chars += 1;
                }

                if remaining_chars != 6 {
                    return false;
                }
            }
            None => return false,
        }

        match self.fields.get("ecl") {
            Some(value) => {
                if !VALID_HAIR_COLORS.contains(&&value[..]) {
                    return false;
                }
            }
            None => return false,
        }

        match self.fields.get("pid") {
            Some(value) => {
                let mut num_chars = 0;
                for ch in value.chars() {
                    if !(ch >= '0' && ch <= '9') {
                        return false;
                    }

                    num_chars += 1;
                }

                if num_chars != 9 {
                    return false;
                }
            }
            None => return false,
        }

        true
    }
}

#[aoc_generator(day4)]
pub fn input_generator(input: &str) -> Vec<Passport> {
    input
        .split("\n\n")
        .map(|info| {
            let mut passport = Passport {
                fields: HashMap::new(),
            };

            for field in info.split_ascii_whitespace() {
                let mut split = field.split(':');
                let name = split.next().unwrap();
                let value = split.next().unwrap();

                passport.fields.insert(name.to_owned(), value.to_owned());
            }

            passport
        })
        .collect()
}

#[aoc(day4, part1)]
pub fn solve_part1(input: &[Passport]) -> usize {
    input
        .iter()
        .filter(|passport| passport.has_required_fields())
        .count()
}

#[aoc(day4, part2)]
pub fn solve_part2(input: &[Passport]) -> usize {
    input.iter().filter(|passport| passport.is_valid()).count()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_valid_passports() {
        const INPUT_STR: &str = "pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719";
        let input = input_generator(INPUT_STR);

        assert!(input[0].is_valid());
        assert!(input[1].is_valid());
        assert!(input[2].is_valid());
        assert!(input[3].is_valid());
    }

    #[test]
    fn test_invalid_passports() {
        const INPUT_STR: &str = "eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007";
        let input = input_generator(INPUT_STR);

        assert!(!input[0].is_valid());
        assert!(!input[1].is_valid());
        assert!(!input[2].is_valid());
        assert!(!input[3].is_valid());
    }
}
