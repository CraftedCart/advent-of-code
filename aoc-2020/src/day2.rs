pub struct PasswordAndPolicy {
    min_occurances: u32,
    max_occurances: u32,
    needed_char: char,
    password: String,
}

#[aoc_generator(day2)]
pub fn input_generator(input: &str) -> Vec<PasswordAndPolicy> {
    input
        .lines()
        .map(|line| {
            let mut split = line.split(' ');

            let min_max = split.next().unwrap();
            let needed_char = split.next().unwrap().chars().next().unwrap();
            let password = split.next().unwrap();

            let mut min_max_split = min_max.split('-');
            let min_occurances = min_max_split.next().unwrap().parse::<u32>().unwrap();
            let max_occurances = min_max_split.next().unwrap().parse::<u32>().unwrap();

            PasswordAndPolicy {
                min_occurances,
                max_occurances,
                needed_char,
                password: password.to_owned(),
            }
        })
        .collect()
}

#[aoc(day2, part1)]
pub fn solve_part1(input: &[PasswordAndPolicy]) -> u32 {
    let mut valid_passwords = 0;

    for item in input {
        let needed_char_occurances = item
            .password
            .chars()
            .filter(|ch| *ch == item.needed_char)
            .count() as u32;
        if needed_char_occurances >= item.min_occurances
            && needed_char_occurances <= item.max_occurances
        {
            valid_passwords += 1;
        }
    }

    valid_passwords
}

#[aoc(day2, part2)]
pub fn solve_part2(input: &[PasswordAndPolicy]) -> u32 {
    let mut valid_passwords = 0;

    for item in input {
        let mut valid_occurances = 0;
        let char1 = item
            .password
            .chars()
            .skip((item.min_occurances - 1) as usize)
            .next()
            .unwrap();
        let char2 = item
            .password
            .chars()
            .skip((item.max_occurances - 1) as usize)
            .next()
            .unwrap();

        if char1 == item.needed_char {
            valid_occurances += 1;
        }
        if char2 == item.needed_char {
            valid_occurances += 1;
        }

        if valid_occurances == 1 {
            valid_passwords += 1;
        }
    }

    valid_passwords
}
