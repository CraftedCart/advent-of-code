#[derive(PartialEq, Eq)]
pub enum Tile {
    Empty,
    Tree,
}

pub struct Map {
    grid: Vec<Vec<Tile>>,
}

impl Map {
    fn get_width(&self) -> usize {
        self.grid[0].len()
    }

    fn get_height(&self) -> usize {
        self.grid.len()
    }
}

#[aoc_generator(day3)]
pub fn input_generator(input: &str) -> Map {
    let grid = input
        .lines()
        .map(|line| {
            line.chars()
                .map(|ch| match ch {
                    '.' => Tile::Empty,
                    '#' => Tile::Tree,
                    _ => panic!("Bad tile!"),
                })
                .collect()
        })
        .collect();

    Map { grid }
}

#[aoc(day3, part1)]
pub fn solve_part1(input: &Map) -> usize {
    get_trees_hit_for_slope(input, 3, 1)
}

#[aoc(day3, part2)]
pub fn solve_part2(input: &Map) -> usize {
    get_trees_hit_for_slope(input, 1, 1)
        * get_trees_hit_for_slope(input, 3, 1)
        * get_trees_hit_for_slope(input, 5, 1)
        * get_trees_hit_for_slope(input, 7, 1)
        * get_trees_hit_for_slope(input, 1, 2)
}

fn get_trees_hit_for_slope(input: &Map, x_increment: usize, y_increment: usize) -> usize {
    let mut pos_x = 0;
    let mut pos_y = 0;

    let mut trees_hit = 0;

    while pos_y < input.get_height() {
        if input.grid[pos_y][pos_x] == Tile::Tree {
            trees_hit += 1;
        }

        pos_x += x_increment;
        pos_y += y_increment;

        if pos_x >= input.get_width() {
            pos_x -= input.get_width();
        }
    }

    trees_hit
}
