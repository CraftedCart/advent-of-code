struct BagNum {
    color: String,
    num: u32,
}

pub struct BagRules {
    color: String,
    must_contain: Vec<BagNum>,
}

impl BagRules {
    fn can_contain_color(&self, all_rules: &[BagRules], color: &str) -> bool {
        for contained in &self.must_contain {
            if contained.color == color {
                return true;
            } else {
                for rules in all_rules {
                    if rules.color == contained.color {
                        if rules.can_contain_color(all_rules, color) {
                            return true;
                        }
                    }
                }
            }
        }

        // Nope!
        false
    }

    fn get_num_bags_inside(&self, all_rules: &[BagRules]) -> u32 {
        let mut num_bags_inside = 0;

        for contained in &self.must_contain {
            num_bags_inside += contained.num;

            for rules in all_rules {
                if rules.color == contained.color {
                    num_bags_inside += contained.num * rules.get_num_bags_inside(all_rules);

                    break;
                }
            }
        }

        num_bags_inside
    }
}

fn split_once<'a>(in_string: &'a str, split_with: &str) -> (&'a str, &'a str) {
    let mut split = in_string.splitn(2, split_with);

    let first = split.next().unwrap();
    let second = split.next().unwrap();
    (first, second)
}

#[aoc_generator(day7)]
pub fn input_generator(input: &str) -> Vec<BagRules> {
    input
        .lines()
        .map(|line| {
            let (color, rest) = split_once(line, " bags contain ");
            let mut must_contain: Vec<BagNum> = Vec::new();

            let must_contain_split = rest.split(", ");
            for must_contain_str in must_contain_split {
                if must_contain_str == "no other bags." {
                    continue;
                } else {
                    let cleaned = must_contain_str
                        .trim_matches(|ch| ch == ' ' || ch == '.')
                        .trim_end_matches(" bags")
                        .trim_end_matches(" bag");
                    let (contain_num, contain_color) = split_once(cleaned, " ");
                    let contain_num = contain_num.parse::<u32>().unwrap();

                    must_contain.push(BagNum {
                        color: contain_color.to_owned(),
                        num: contain_num,
                    });
                }
            }

            BagRules {
                color: color.to_owned(),
                must_contain,
            }
        })
        .collect()
}

#[aoc(day7, part1)]
pub fn solve_part1(input: &[BagRules]) -> usize {
    let mut matching_rules = 0;
    for rules in input {
        if rules.can_contain_color(input, "shiny gold") {
            matching_rules += 1;
        }
    }

    matching_rules
}

#[aoc(day7, part2)]
pub fn solve_part2(input: &[BagRules]) -> u32 {
    for rules in input {
        if rules.color == "shiny gold" {
            return rules.get_num_bags_inside(input);
        }
    }

    panic!("No matching bag found!")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_num_bags_inside() {
        const INPUT_STR: &str = "shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.";
        let all_rules = input_generator(INPUT_STR);

        for rules in &all_rules {
            if rules.color == "shiny gold" {
                assert_eq!(rules.get_num_bags_inside(&all_rules), 126);
                return;
            }
        }
    }
}
