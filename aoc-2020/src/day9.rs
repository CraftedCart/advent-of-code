#[aoc_generator(day9)]
pub fn input_generator(input: &str) -> Vec<u64> {
    input
        .lines()
        .map(|line| line.parse::<u64>().unwrap())
        .collect()
}

fn can_two_numbers_add_to_number(nums: &[u64], target_number: u64) -> bool {
    for num1 in nums {
        for num2 in nums {
            if num1 + num2 == target_number && num1 != num2 {
                return true;
            }
        }
    }

    false
}

fn find_first_non_matching_number(nums: &[u64], rolling_size: usize) -> u64 {
    for i in rolling_size..nums.len() {
        if !can_two_numbers_add_to_number(&nums[i - rolling_size..i], nums[i]) {
            return nums[i];
        }
    }

    panic!("No non-matching numbers found")
}

fn find_contiguous_range_that_sums_to_number(nums: &[u64], target_number: u64) -> &[u64] {
    for i in 0..nums.len() {
        let mut range = 1;

        loop {
            let sum: u64 = nums[i..i + range].iter().sum();

            if sum == target_number {
                return &nums[i..i + range];
            } else if sum > target_number {
                break;
            } else {
                range += 1;
            }
        }
    }

    panic!("No contiguous range found")
}

#[aoc(day9, part1)]
pub fn solve_part1(input: &[u64]) -> u64 {
    find_first_non_matching_number(input, 25)
}

#[aoc(day9, part2)]
pub fn solve_part2(input: &[u64]) -> u64 {
    let nums =
        find_contiguous_range_that_sums_to_number(input, find_first_non_matching_number(input, 25));
    let smallest = nums.iter().min().unwrap();
    let biggest = nums.iter().max().unwrap();

    smallest + biggest
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_first_non_matching_number() {
        const INPUT_STR: &str = "5
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576";
        let nums = input_generator(INPUT_STR);

        assert_eq!(find_first_non_matching_number(&nums, 5), 127);
    }

    #[test]
    fn test_first_contiguous_range_that_sums_to_number() {
        const INPUT_STR: &str = "5
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576";
        let nums = input_generator(INPUT_STR);

        assert_eq!(
            find_contiguous_range_that_sums_to_number(&nums, 127),
            [15, 25, 47, 40]
        );
    }
}
