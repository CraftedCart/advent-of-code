module Day2 (part1, part2) where

import Data.List (tails)

-- Convert a string like...
-- 5 1 9 5
-- 7 5 3
-- 2 4 6 8
-- ...into a list of lists like...
-- [[5, 1, 9, 5],
--  [7, 5, 3],
--  [2, 4, 6, 8]]
parse :: String -> [[Int]]
parse str = map (map read) (map words (lines str))

-- Gets the difference between the min and max numbers for each row, and sums them
part1 :: String -> Int
part1 str = sum $ map (\row -> (maximum row) - (minimum row)) spreadsheet
  where
    spreadsheet = parse str

-- Returns pairs of elements in a list
-- Eg: For a list like `[1, 2, 3]`, this yeilds `[(1, 2), (1, 3), (2, 3)]`
--
-- I think I understand how this works just about, but I pinched this from Stack Overflow ;P
pairs :: [a] -> [(a, a)]
pairs list = [(x, y) | (x:ys) <- tails list, y <- ys]

-- Finds a divisible pair in a list, like `(12, 3)`, and divides it
findDivisiblePair :: [(Int, Int)] -> Int
findDivisiblePair ((left, right):rest)
  | left `mod` right == 0 = left `div` right
  | right `mod` left == 0 = right `div` left
  | otherwise = findDivisiblePair rest
findDivisiblePair [] = undefined

-- Gets the result of dividing the only two divisible numbers in each row, and sums them
-- (the only two numbers which divide cleanly into a whole number)
part2 :: String -> Int
part2 str = sum $ map findDivisiblePair (map pairs spreadsheet)
  where
    spreadsheet = parse str
