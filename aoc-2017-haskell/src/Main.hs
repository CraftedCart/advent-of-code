module Main (main) where

import Day1
import Day2

main :: IO ()
main = do
  readFile "input/day1.txt" >>= \input -> do
    putStrLn $ "Day1 Part1: " ++ (show $ Day1.part1 input)
    putStrLn $ "Day1 Part2: " ++ (show $ Day1.part2 input)

  readFile "input/day2.txt" >>= \input -> do
    putStrLn $ "Day2 Part1: " ++ (show $ Day2.part1 input)
    putStrLn $ "Day2 Part2: " ++ (show $ Day2.part2 input)
