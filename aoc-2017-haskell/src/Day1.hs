-- This is probably pretty horrible Haskell if I were to guess
-- but I'm new and I'm just happy I figured something out ;P

module Day1 (part1, part2) where

import Data.Char (digitToInt)

-- Converts a string like "1234" into [1, 2, 3, 4]
strToDigits :: String -> [Int]
strToDigits = map digitToInt

sumDigitsWithIdenticalNextDigit :: [Int] -> Int
sumDigitsWithIdenticalNextDigit digits = sumDigitsWithIdenticalNextDigit' 0 (digits ++ [head digits])
  where
    sumDigitsWithIdenticalNextDigit' accumulator (first:second:rest)
      = accumulator
      + (if first == second then first else 0) -- If the current digit is equal to the next one, add it
      + (sumDigitsWithIdenticalNextDigit' accumulator (second:rest)) -- Recurse for the next digit

    -- Stop looping when we run out of digits to check
    sumDigitsWithIdenticalNextDigit' accumulator _ = accumulator

sumDigitsWithIdenticalDigitAtOffset :: Int -> [Int] -> Int
sumDigitsWithIdenticalDigitAtOffset offset digits = sumDigitsWithIdenticalDigitAtOffset' 0 0 (cycle digits)
  where
    -- Stop looping once we've seen the whole list (before it cycles around again)
    sumDigitsWithIdenticalDigitAtOffset' accumulator index _ | offset * 2 == index = accumulator

    sumDigitsWithIdenticalDigitAtOffset' accumulator index xs
      = accumulator
      + (if first == (xs !! offset) then first else 0) -- If the current digit is equal to the digit halfway round the list, add it
      + (sumDigitsWithIdenticalDigitAtOffset' accumulator (index + 1) (tail xs)) -- Recurse for the next digit
      where
        first = head xs

part1 :: String -> Int
part1 input =
  sumDigitsWithIdenticalNextDigit digits
  where
    digits = strToDigits input

part2 :: String -> Int
part2 input =
  sumDigitsWithIdenticalDigitAtOffset halfLength digits
  where
    digits = strToDigits input
    halfLength = (length digits) `div` 2
